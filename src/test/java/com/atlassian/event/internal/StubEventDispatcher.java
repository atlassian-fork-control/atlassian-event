package com.atlassian.event.internal;

import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.event.spi.ListenerInvoker;

import javax.annotation.Nonnull;

/**
 * A stub event dispatcher that simply calls the invoker.
 */
final class StubEventDispatcher implements EventDispatcher {
    public void dispatch(@Nonnull ListenerInvoker invoker, @Nonnull Object event) {
        invoker.invoke(event);
    }
}
