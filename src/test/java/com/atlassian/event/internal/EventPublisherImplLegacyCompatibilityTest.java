package com.atlassian.event.internal;

import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import com.atlassian.event.EventManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.legacy.LegacyEventManager;
import com.atlassian.event.legacy.LegacyListenerHandler;
import com.atlassian.event.spi.ListenerHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

import static com.atlassian.event.internal._TestEvent.TestInterface;
import static com.atlassian.event.internal._TestEvent.TestInterfacedEvent;
import static com.atlassian.event.internal._TestEvent.TestSubEvent;
import static com.atlassian.event.internal._TestEvent.TestSubSubEvent;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * <strong>Note:</strong> This is more of a (small) integration test as we use a concrete
 * {@link com.atlassian.event.legacy.LegacyListenerHandler} at runtime here (and not a stub/mock). It essentially
 * ensure that we get the same behavior at the legacy event manager.
 */
@SuppressWarnings("deprecation")
public class EventPublisherImplLegacyCompatibilityTest {
    private EventManager eventManager;
    private EventPublisher eventPublisher;

    @Before
    public final void setUp() {
        eventPublisher = new EventPublisherImpl(new StubEventDispatcher(), new ListenerHandlersConfiguration() {
            @Nonnull
            public List<ListenerHandler> getListenerHandlers() {
                return Collections.singletonList(new LegacyListenerHandler());
            }
        });
        eventManager = new LegacyEventManager(eventPublisher);
    }

    @After
    public final void tearDown() {
        eventManager = null;
        eventPublisher = null;
    }

    @Test(expected = NullPointerException.class)
    public final void testPublishNullEvent() {
        eventManager.publishEvent(null);
    }

    @Test
    public final void testRegisterListener() {
        final Event event = new _TestEvent(this);
        final EventListener listener = getListener(_TestEvent.class);

        eventManager.registerListener("key", listener);

        verifyEventReceived(event, listener);
    }

    @Test
    public final void testListenerWithoutMatchingEventClass() {
        final Event event = getEvent();
        final EventListener listener = getListener(_TestEvent.class);

        eventManager.registerListener("key1", listener);
        eventManager.publishEvent(event);

        verify(listener, never()).handleEvent(event);
    }

    @Test
    public final void testUnregisterListener() {
        final Event event = new _TestEvent(this);
        final EventListener listener = getListener(_TestEvent.class);

        eventManager.registerListener("key", listener);
        eventManager.publishEvent(event);
        eventPublisher.publish(event);

        verify(listener, times(2)).handleEvent(event);

        eventManager.unregisterListener("key");
        eventManager.publishEvent(event);
        eventPublisher.publish(event);

        verify(listener, times(2)).handleEvent(event); // i.e it's no called again
    }

    @Test
    public final void testListensForEverything() {
        final Event event = new _TestEvent(this);
        final EventListener listener = getListener();

        eventManager.registerListener("key", listener);
        verifyEventReceived(event, listener);
    }

    @Test
    public final void testListensForEverythingDoesNotReceiveNonEventClasses() {
        final EventListener listener = getListener();

        eventManager.registerListener("key", listener);
        eventPublisher.publish("Cheese");

        verify(listener, never()).handleEvent(any(Event.class));
    }

    @Test
    public final void testRemoveNonExistentListener() {
        final EventListener listener = getListener(Event.class);

        eventManager.registerListener("key", listener);
        eventManager.unregisterListener("this.key.is.not.registered");
    }

    @Test
    public final void testDuplicateKeysForListeners() {
        final String key = "key1";
        final Event event = new _TestEvent(this);
        final EventListener listener1 = getListener(_TestEvent.class);
        final EventListener listener2 = getListener(_TestEvent.class);

        eventManager.registerListener(key, listener1);
        eventManager.registerListener(key, listener2);

        eventManager.publishEvent(event);
        eventPublisher.publish(event);

        verify(listener1, never()).handleEvent(event);
        verify(listener2, times(2)).handleEvent(event);

        eventManager.unregisterListener(key);
        eventManager.publishEvent(event);
        eventPublisher.publish(event);

        verify(listener1, never()).handleEvent(event);
        verify(listener2, times(2)).handleEvent(event); // i.e. it's not been called again
    }

    @Test(expected = NullPointerException.class)
    public final void testAddValidKeyWithNullListener() {
        eventManager.registerListener("key1", null);
    }

    @Test
    public final void testInterfacesHandled() {
        final Event event = new TestInterfacedEvent(this);
        final EventListener listener = getListener(TestInterface.class);

        eventManager.registerListener("key1", listener);
        verifyEventReceived(event, listener);
    }

    @Test
    public final void testChildrenHandled() {
        final Event event = new TestSubEvent(this);
        final EventListener listener = getListener(_TestEvent.class);

        eventManager.registerListener("key1", listener);
        verifyEventReceived(event, listener);
    }

    @Test
    public final void testGrandChildrenHandled() {
        final Event event = new TestSubSubEvent(this);
        final EventListener listener = getListener(_TestEvent.class);

        eventManager.registerListener("key1", listener);
        verifyEventReceived(event, listener);
    }

    @Test
    public final void testOneEventForTwoHandledClasses() {
        final Event event = new TestSubSubEvent(this);
        final EventListener listener = getListener(_TestEvent.class, TestSubEvent.class);

        eventManager.registerListener("key1", listener);
        eventManager.publishEvent(event);

        verify(listener, times(1)).handleEvent(event);
    }

    protected Event getEvent() {
        return new Event(this) {
        };
    }

    private void verifyEventReceived(final Event event, final EventListener listener) {
        eventManager.publishEvent(event);
        eventPublisher.publish(event);
        verify(listener, times(2)).handleEvent(event);
    }

    private EventListener getListener(final Class<?>... classes) {
        final EventListener listener = mock(EventListener.class);
        when(listener.getHandledEventClasses()).thenReturn(classes);
        return listener;
    }
}
