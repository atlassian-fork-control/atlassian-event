package com.atlassian.event.spring;

import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.ListenerHandler;
import com.atlassian.event.spi.ListenerInvoker;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collection;
import java.util.Map;

import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class EventListenerRegistrarBeanProcessorTest {
    private static final String REGISTRAR_BEAN_NAME = "registrar";

    private EventListenerRegistrarBeanProcessor registrarBeanProcessor;
    @Mock
    private ListenerHandlersConfiguration listenerHandlersConfiguration;
    @Mock
    private EventListenerRegistrar eventListenerRegistrar;
    @Mock
    private ConfigurableBeanFactory beanFactory;
    @Mock
    private BeanDefinition beanDefinition;
    @Mock
    private ListenerHandler listenerHandler;
    @Mock
    private ListenerInvoker listenerInvoker;
    @Mock
    private ModuleDescriptor moduleDescriptor;
    @Mock
    private Plugin plugin;
    private PluginModuleEnabledEvent pluginModuleEnabledEvent;
    private final SomeListenerClass listenerBean = new SomeListenerClass();

    @Before
    public void setup() {
        registrarBeanProcessor = new EventListenerRegistrarBeanProcessor(REGISTRAR_BEAN_NAME, listenerHandlersConfiguration);

        // Default behaviour for our tests is that a BeanFactory is present, that when asked for the BeanDefintion
        // it returns one, and that the BeanDefintion says that all beans are singletons.
        // Individual tests may override this behaviour.
        registrarBeanProcessor.setBeanFactory(beanFactory);
        when(beanFactory.getMergedBeanDefinition(isA(String.class))).thenReturn(beanDefinition);
        when(beanFactory.getMergedBeanDefinition(null)).thenThrow(new NoSuchBeanDefinitionException("null bean name"));
        when(beanDefinition.isSingleton()).thenReturn(true);

        // Set up a single ListenerHandler
        when(listenerHandlersConfiguration.getListenerHandlers()).thenReturn(singletonList(listenerHandler));
        when(listenerHandler.getInvokers(listenerBean)).thenAnswer(invocation -> singletonList(listenerInvoker));

        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        when(moduleDescriptor.getModuleClass()).thenReturn(Object.class);
        when(moduleDescriptor.getModule()).thenReturn(listenerBean);
        when(plugin.getKey()).thenReturn("com.atlassian.test:some.plugin");

        pluginModuleEnabledEvent = new PluginModuleEnabledEvent(moduleDescriptor);
    }

    @Test
    public void testThatTheListenerIsRegisteredImmediatelyIfTheRegistrarIsAvailable() {
        processTheRegistrarProcessorBean();
        processTheListenerBean();

        verifyThatTheListenerHasBeenRegistered();
        verifyThatNoMoreListenersNeedToBeRegistered();
    }

    @Test
    public void testThatTheListenerIsRememberedForLaterRegistrationIfTheRegistrarIsNotAvailable() {
        processTheListenerBean();

        verifyNoMoreInteractions(eventListenerRegistrar);
        assertThat(listenersToBeRegistered(), hasItem(listenerBean));

        processTheRegistrarProcessorBean();

        verifyThatTheListenerHasBeenRegistered();
        verifyThatNoMoreListenersNeedToBeRegistered();
    }

    @Test
    public void testThatNonListenerBeansAreIgnored() {
        when(listenerHandler.getInvokers(listenerBean)).thenReturn(EMPTY_LIST);
        processTheRegistrarProcessorBean();
        processTheListenerBean();

        verifyNoMoreInteractions(eventListenerRegistrar);
        verifyThatNoMoreListenersNeedToBeRegistered();
    }

    @Test
    public void blacklistedPluginsAreNotRegistered() {
        when(plugin.getKey()).thenReturn("com.atlassian.activeobjects.activeobjects-plugin");

        registrarBeanProcessor.onPluginModuleEnabled(pluginModuleEnabledEvent);
        verifyThatNoMoreListenersNeedToBeRegistered();
    }

    @Test
    public void validListenerModuklesAreRegisteredAsListeners() {
        registrarBeanProcessor.onPluginModuleEnabled(pluginModuleEnabledEvent);
        processTheRegistrarProcessorBean();
        verifyThatTheListenerHasBeenRegistered();
    }

    private Collection<Object> listenersToBeRegistered() {
        final Map<String, Object> listenersToBeRegistered =
                (Map<String, Object>) ReflectionTestUtils.getField(registrarBeanProcessor, "listenersToBeRegistered");
        return listenersToBeRegistered.values();
    }

    private void verifyThatNoMoreListenersNeedToBeRegistered() {
        assertThat(listenersToBeRegistered(), is(hasSize(0)));
    }

    private void verifyThatTheListenerHasBeenRegistered() {
        verify(eventListenerRegistrar).register(listenerBean);
    }

    private void processTheListenerBean() {
        registrarBeanProcessor.postProcessBeforeInitialization(listenerBean, "listener");
    }

    private void processTheRegistrarProcessorBean() {
        registrarBeanProcessor.postProcessAfterInitialization(eventListenerRegistrar, REGISTRAR_BEAN_NAME);
    }

    private class SomeListenerClass {
    }
}
