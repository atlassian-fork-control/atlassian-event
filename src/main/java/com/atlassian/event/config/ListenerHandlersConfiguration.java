package com.atlassian.event.config;

import com.atlassian.event.spi.ListenerHandler;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Specifies a listener handler configuration to use
 */
public interface ListenerHandlersConfiguration {
    @Nonnull
    List<ListenerHandler> getListenerHandlers();
}
