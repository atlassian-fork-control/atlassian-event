package com.atlassian.event.internal;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.plugin.scope.ScopeManager;

import javax.annotation.Nonnull;

/**
 * A non-blocking implementation of the {@link com.atlassian.event.api.EventPublisher} interface.
 *
 * @since 2.0.2
 *
 * @deprecated since 4.0.3 use {@link EventPublisherImpl} directly.
 */
@Deprecated
public final class LockFreeEventPublisher implements EventPublisher {
    private final EventPublisher delegate;

    public LockFreeEventPublisher(EventDispatcher eventDispatcher,
                                  ListenerHandlersConfiguration configuration,
                                  ScopeManager scopeManager) {
        this(eventDispatcher, configuration);
    }

    public LockFreeEventPublisher(EventDispatcher eventDispatcher,
                                  ListenerHandlersConfiguration configuration) {
        this(eventDispatcher, configuration, (invokers, event) -> invokers);
    }

    public LockFreeEventPublisher(EventDispatcher eventDispatcher,
                                  ListenerHandlersConfiguration listenerHandlersConfiguration,
                                  InvokerTransformer transformer) {
        this.delegate = new EventPublisherImpl(eventDispatcher, listenerHandlersConfiguration, transformer);
    }

    @Override
    public void publish(@Nonnull Object event) {
        delegate.publish(event);
    }

    @Override
    public void register(@Nonnull Object listener) {
        delegate.register(listener);
    }

    @Override
    public void unregister(@Nonnull Object listener) {
        delegate.unregister(listener);
    }

    @Override
    public void unregisterAll() {
        delegate.unregisterAll();
    }
}
