package com.atlassian.event;
/**
 * This package contains the legacy event system public classes and interfaces. Those are all deprecated and one
 * should rely on interfaces in the {@link com.atlassian.event.api} package instead.
 * <p>
 * Classes in this package will be removed in a future version of Atlassian Event
 */