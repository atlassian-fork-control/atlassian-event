# Atlassian Event

## Description

Standard event listening, registration, and publication interface, along with basic implementations.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/EVENT)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/EVENT)

### Documentation

- [JIRA Event Listener Documentation](https://developer.atlassian.com/x/BgIr)
- [Confluence Event Listener Documentation](https://developer.atlassian.com/x/2gAf)
